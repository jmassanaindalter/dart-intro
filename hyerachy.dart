


void main(){

    Superman superman = Superman(name:"Clark Ken");
    superman.describe();

    superman.takeOff();


    Vehicle myVehicle = Plane(name:"Air Force One",speed: 1000,capacity: 120,weight: 20000);


    if(myVehicle is Plane){
      myVehicle.takeOff();
    }
}


mixin Fly{

    takeOff(){
      print("I'm taking off");
    }

    land(){
      print("I'm landing");
    }

}




mixin Swim{

  startSwimming(){
    print("I'm swiming");
  }

  stopSwimming(){
    print("I'm swiming");
  }


}


mixin Run{

  startRunning(){
    print("I'm running");
  }

  accelerate(){
    "I'm accelerating";
  }

  stopRunning(){
    "Run stop";
  }


}


abstract class Info{

  void describe();

}


abstract class Character{
  String name;
  int health;
  int power;
  int coins;

  Character({
      required this.name,
      required this.health,
      required this.power,
      required this.coins});



}

class Aquaman extends Character  with Run,Swim implements Info{


  Aquaman({ required name  })
      :super(name:name,health:100,power:300,coins:0);

  @override
  void describe(){
    print("My name is $name and I'm Fishman");
  }

}


class Batman extends Character with Run,Swim implements Info{

  Batman({ required name  })
      :super(name:name,health:100,power:300,coins:0);


  @override
  void describe(){
    print("My name is $name and I'm Batman");
  }


}

class Superman extends Character with Run,Fly implements Info{


  Superman({ required name })
      :super(name:name,health:100,power:400,coins:0);


  @override
  void describe(){
    print("My name is $name and I'm Superman");
  }

}




abstract class Vehicle implements Info{
  String name;
  int speed;
  int capacity;
  int weight;

  Vehicle({required this.name,this.speed = 120,this.capacity = 4,this.weight = 10});

  @override
  describe(){
    "I'm a vehicle";
  }
}


class Plane extends Vehicle with Run,Fly {

  Plane({ required name,speed,capacity,weight })
      :super(name:name,speed:speed,capacity:capacity);

  @override
  void describe(){
    print("I'm a plane");
  }

}

class Car extends Vehicle with Run {

  Car({ required name,speed,capacity,weight })
      :super(name:name,speed:speed,capacity:capacity);

  @override
  void describe(){
    print("I'm a car");
  }

}

