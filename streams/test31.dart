// ANONYM FUNCTIONS
//ABSTRACT CLASSES

// LIBRARIES

//STREAMS
import 'dart:async';
import 'dart:io';

import 'package:intl/intl.dart';


void main() async{

    var streamController = StreamController<List<String>>();


    streamController.stream.listen((data) {

      if(streamController.hasListener){
        for(String element in data){
          print("element $element");
        }
      }


    },
    onError:(error) => print("There was an error on the stream $error"),
    onDone: () => print("Stream finished successfully"));


    streamController.add(["You","They","She","He","it"]);

    streamController.add(["Car","Truck"]);

    streamController.addError('500');

    streamController.close();

    streamController.add([]);

}





