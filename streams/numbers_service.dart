import 'dart:async';

import 'dart:math';


// CREAR L'STREAM
// FER L'STRWAM CONTROLLER NO BROADCAST I VEURE QUE DONA ERROR;

class NumberService{



  StreamController<int> numbersStream = StreamController.broadcast(
    onListen: (){
      print("new user listening");
    },
    onCancel: (){
      print("stream cancelled");
    }

  );



  void initService(){

    Timer.periodic(Duration(seconds:2), (timer) {

      if(numbersStream.hasListener){

        var num = Random().nextInt(200);
        print("sending number:$num");

        numbersStream.sink.add(num);


      }else{
        print("There are no subscribers");
      }

    });

  }


}

//FER


void main(){
      var nService = NumberService();

      nService.initService();

      Timer(Duration(seconds:1),(){
        var subscription1 = nService.numbersStream.stream.listen((event) {
          print("I'm subscriber 1 and I'm printing ${event}");
        });
      });

//DONA ERROR
      Timer(Duration(seconds:2),(){
        var subscription2 = nService.numbersStream.stream.map(
            (element)=>  element+1


        ).listen((event) {

          print("I'm subscriber 2 and I'm printing ${event}");
        });
      });






}