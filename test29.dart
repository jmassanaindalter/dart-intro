// ANONYM FUNCTIONS
//ABSTRACT CLASSES

// LIBRARIES
import 'dart:io';

import "lib/areas.dart" as areas;

void main() async{

  File('./file.txt').createSync();

  File file = File('./file.txt');

  String content = "";

  if(await file.exists()){
    print("FILE EXISTS");

    await file.writeAsString("Dart is great\n");
    content = await file.readAsString();


  //CHECK WITHOUT MODE
    var sink = file.openWrite(mode:FileMode.append);
    sink.writeln("I've open the file for writting");
    sink.writeln("I've open the file for writting again");

    await sink.close();

    content = await file.readAsString();

    print(content);


  }else{
    print("FILE DOES NO EXIST");
  }

}





