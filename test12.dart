// TEST OPERATORS
//FOR CYCLE AND LOOPS
void main(){

  for(int i = 0; i < 10; i++){
    print("THIS IS NUMBNER: $i");
    if(i == 3) {
      continue;
    }
    if(i == 5){
      break;
    }
    //break;
    print("THIS IS AFTER NUMNBER $i");
  }


  var option = "Beans";


  switch(option.toLowerCase()){
    case "beans":
      print("I HAVE $option today");
      break;
    case "apple":
      print("I have apple");
      break;
    case "orange":
      print("I have orange");
      break;
    default:
      print("I have nothing from known options");
  }
}