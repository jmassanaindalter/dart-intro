// ANONYM FUNCTIONS
//CLOSURES

// A CLOUSURE IS AN OBJECT THAT HAS ACCESS TO THE LEXIC SCOPE

//FROM OFFICIAL DART DOCUMENTAITION
Function makeAdder(num addBy){
  return (num i)=>addBy + i;
}

void message(){

  String msg = "Hey there";

  void newMsg(){

    msg = "hey there from Dart";
    print(msg);

  };

  return newMsg();

}

void main(){
  var add2 = makeAdder(2);

  //assert(add2(3) == 4);

  //assert(add2(3) == 5);


  message();



}






