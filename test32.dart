// ANONYM FUNCTIONS
//ABSTRACT CLASSES

// LIBRARIES

//STREAMS


import 'dart:async';
import 'dart:io';
import 'dart:isolate';

void main() async {
  print("STarting crono");
  await start();

  Future.delayed(const Duration(seconds:10),(){
      stop();
      print("crono finished");
      exit(0);
  });
 /* print("PRESS ENTER TO FINISH");
  await stdin.first;
  */
}

Isolate? isolate;

start() async {

  ReceivePort receivePort = ReceivePort();

  isolate = await Isolate.spawn(crono, receivePort.sendPort);

  receivePort.listen(messageManager, onDone: () => print("Done"));


}

crono(SendPort sendPort) async{
  int seconds = 0;
  int minutes = 0;

  Timer.periodic(Duration(seconds: 1), (timer) {
      seconds++;
      if(seconds == 60){
        seconds = 0;
        minutes++;
      }

      String msg = "$minutes : $seconds";
      print("MSG IN CRONO: $msg");
      sendPort.send(msg);
  });


}

messageManager(dynamic data){
  print("Elapsed time: $data");
}


stop(){
  if(isolate != null){
    print("FINISHING CRONO");
    isolate?.kill(priority: Isolate.immediate);
    isolate = null;
  }
}



