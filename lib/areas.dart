library areas_perimeters;
import 'dart:math' as math;

double triangle({ required double base, required double height}){
    return (base / height) / 2;
}

double square({required double side}) => side * side;

double circle({required double radius}) => math.pi * radius;
