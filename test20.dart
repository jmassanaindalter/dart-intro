// ANONYM FUNCTIONS
//ABSTRACT CLASSES

// A CLOUSURE IS AN OBJECT THAT HAS ACCESS TO THE LEXIC SCOPE


abstract class Transport {
    int doors = 100;
    int wheels = 4;

    void speed(){
      print("120 Km/h");
    }
}

class Trailer implements Transport{
  @override
  int doors = 2;

  @override
  int wheels = 16;

  @override
  void speed() {
    print("150Km/h");
    // TODO: implement speed
  }

}


class Motorbike implements Transport{
  @override
  int doors = 0;

  @override
  int wheels = 2;

  @override
  void speed() {
    print("250Km/h");
    // TODO: implement speed
  }

}


int howManyWheels(Transport t){
  return t.wheels;
}


void main(){
    Trailer t = Trailer();
    print("${howManyWheels(t)}");
}






