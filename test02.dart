import 'variables.dart' as variables;

void main(){

  //Check that the assignment defines the type of var
  var something = 3.4;
  //WARNING - something = "HEY";

  variables.intVar = 4;

  //type dynamic can change
  dynamic dynamicVar = "hola";
  dynamicVar = 3.15;

  var result = addOneAndReturn(dynamicVar);


  print("RESULT IS $result");

}


double addOneAndReturn(double a){
  return a+1.0;
}


//EXCERCISE CREATE A FUNCTION THAT CONVERTS LBs TO KGs ( 1Lb = 0,45359237 Kg)