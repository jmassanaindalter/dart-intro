void main(){

  Map<String,int> theNumbersMap = {'one':1,'two':2,'three':3};


  List<Map<String,String>> numbers = [
    {
      "spanish":"hola",
      "english":"hello",
      "french":"alo"
    },
    {
      "spanish":"rojo",
      "english":"red",
      "french":"rouge"
    },
  ];

  Map<String,String> translations = {'one':'uno','two':'dos','three':'tres'};

  translations.addAll({'four':'cuatro'});

  print("${translations['one']}");


  translations.forEach((key, value) {
    print("El valor en español de la palabra $key es $value");
  });

  translations.update("two", (value) => "es un dos");
  //REMOVE WHERE  get_version: ^0.2.2


  //FILTER

  numbers.where((element) => element['english'] == 'red');

  //EJERCICIO, PASSAR MAP NUMBERS A CLASSE y CREAR UN TIPO


}