// ANONYM FUNCTIONS
//ABSTRACT CLASSES

// LIBRARIES AND ASYNC

// ACCESS TO https://pub.dev

//install some packages


//JSON placeholder
//JSON placeholder

//DART WORKS IN A SINGLE THREAD

import 'dart:convert';

import 'package:dio/dio.dart' as dio;
import 'package:http/http.dart' as http;
Future<void> main() async {
  print("Performing request");
  //await getStatusCode();
  getStatusCode();
  print("......");
}

//WHY THE POINTS GET FIRST

Future<int> postStatusCode() async {
  String data = jsonEncode({
    "name":"Jordi Massana",
    "phone": "699866435"
  });

  http.Response response = await http.post(Uri.parse(""),body: data);

  return response.statusCode;
}

/*
{
  "id": 1,
  "name": "Leanne Graham",
  "username": "Bret",
  "email": "Sincere@april.biz",
  "address": {
    "street": "Kulas Light",
    "suite": "Apt. 556",
    "city": "Gwenborough",
    "zipcode": "92998-3874",
    "geo": {
      "lat": "-37.3159",
      "lng": "81.1496"
    }
  },
  "phone": "1-770-736-8031 x56442",
  "website": "hildegard.org",
  "company": {
    "name": "Romaguera-Crona",
    "catchPhrase": "Multi-layered client-server neural-net",
    "bs": "harness real-time e-markets"
  }
}
 */


Future<int> getStatusCode() async {
    http.Response response = await http.get(Uri.parse("https://jsonplaceholder.typicode.com/users/1"));



    print("RESPONSE STATUS CODE: ${response.statusCode}");
    if(response.statusCode == 200){

      final jsonData = jsonDecode(response.body);

      Map<String,dynamic> mapData = jsonData;

      print("USER NAME: ${mapData['name']}");
      print("USER PHONE: ${mapData['phone']}");


      print("REQUEST WAS SUCCESSFUL");
      return response.statusCode;
    }else{
      print("REQUEST RETURNED AN ERROR");
      return response.statusCode;
    }
}

//EXERCISE CREATE FORM FACTORY;







