// EXERCISE 0 INITIALISE RANDOMLY AN ARRAY OF 100 Characters with random
//properties and finally

// EXERCISE 1: get an array of the most powerful ones power > 30
// EXERCISE 2: count how many characters has the same name
// EXERCISE 3: create array with unique name elements ( name is same)
// EXERCISE 4: create a map (Map<String,List<String>>) where key is the surname with a list
//             of characters name who have that surname
//              { 'Miller':['Arthur Miller', 'Robert Miller','John Miller'...],
//                'Redford':['Robert Redford','Arthur Redford'.....]}


// EXERCISE 5: create a map where key is the property (fly,run,swim) with a list
//             of characters with those properties
//              { 'Fly':['Arthur Miller', 'Robert Miller','John Miller'...],
//                'Swim':['Robert Redford','Arthur Redford'.....],
//                'Run':['Arthur Redford','John Cameron']}
// EXERCISE 6:

//types can be batman, superman or aquaman
//names: [Arthur, Robert, James, John]
//surnames: [ Miller, Redford, Cameron, Cusack]
//power: 1- 100
//health: 0
//coins:0
//name:

//    {
//       "name":"jordi",
//       "surname":"massana"
//    }

import 'dart:math';
import 'heros/batman.dart';
import 'heros/aquaman.dart';
import 'heros/mixins.dart';
import 'heros/superman.dart';
import 'heros/character.dart';



void main(){


  var herosList = initHeros(10);



  //exercise01(herosList);
  exercise02(herosList);
  //exercise03(herosList);
  //exercise04(herosList);
  //exercise05(herosList);

}


void exercise01(List<Character> heros){
    List<Character> filtered = heros.where((element) => element.power > 30).toList();

    heros.forEach((element) {
      element.describe();
    });

    print("There are ${filtered.length} heros with more than 30 power");

}

void exercise02(List<Character> heros){

  Map<String,int> nameCheck = {};

  heros.forEach((element) {

    nameCheck[element.name] = (nameCheck[element.name] ?? 0) + 1;

  });

  print("Name check: $nameCheck");

}

void exercise03(List<Character> heros){

  Set<String> uniqueNames = {};

  heros.forEach((element) {
      uniqueNames.add(element.name);
  });

  print("UNIQUE NAMES: ${uniqueNames}");


}

void exercise04(List<Character> heros){
  Map<String,List<String>> surnamesMap = {};

  heros.forEach((element) {

    //GET SURNAME
    var surname = element.name.split(" ")[1];

    if(!surnamesMap.containsKey(surname)){
      surnamesMap[surname] = [element.name];
    }else{
      surnamesMap[surname]?.add(element.name);
    }

  });
}

void exercise05(List<Character> heros){
  Map<String,List<Character>> canDoMap = {"fly":[],"swim":[],"run":[]};

  heros.forEach((element) {

    //GET SURNAME
    if(element is Fly){
        canDoMap["fly"]?.add(element);
    }

    if(element is Run){

      canDoMap["run"]?.add(element);
    }

    if(element is Swim){
      canDoMap["swim"]?.add(element);
    }

  });

  print("canDoMap: $canDoMap");
}







List<Character> initHeros(int maxHeros){

  List<Character> heros = [];

  var random = Random();

  for(var i = 0; i < maxHeros; i++){
      //0 Batman //1 Superman //2 Aquaman
      var type = random.nextInt(3);
      var power= random.nextDouble()*100+1;
      var name = getName();

      switch (type){
        case 0:
          heros.add(Batman(name:name,power:power));
          break;
        case 1:
          heros.add(Aquaman(name:name,power:power));
          break;
        case 2:
          heros.add(Superman(name:name,power:power));
          break;
      }
  }

  return heros;
}

String getName(){
  var names =  ["Arthur", "Robert", "James", "John"];
  var surnames = [ "Miller", "Redford", "Cameron", "Cusack"];

  var rand = Random();

  return names[rand.nextInt(names.length)]
              + " " + surnames[rand.nextInt(surnames.length)];


}

