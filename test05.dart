main(){

  //DART HAS NIO ARRAYS -- ONLY LISTS

  List<int> occupiedDays = [1,12,15,28];
  List<String> busyReason = ["Fiesta","Comida de empresa","Viaje a Japón","Reunión directivos"];

  List dList = [3,4,5,"Hey","there",0.34];


  print(occupiedDays);

  //PRINT ACCESSING TO SPECIFIC POSITION
  print("busyReason: ${busyReason[0]}");

  print("Position last ${busyReason[5]}"); // CRASHES OVERFLOW

  busyReason.add("Natación");


  busyReason.addAll(['Guardería','Reunión de pades y madres']);


  //LOOPS

  for (var element in busyReason) {
    print("ELEMENT $element");
  }

  busyReason.forEach((element)=>{
    print("ELEMENT $element")
  });

  print("REVERSED: ${busyReason.reversed}");

  //INSERTAT ELEMENTOS
  busyReason.insert(4, "Metido en medio");
  busyReason.insertAll(4, ["Y uno más","Y otro más"]);
  busyReason.removeAt(3);


  //CHECK LENGTH AND REMOVE

  if(busyReason.length > 4){
      busyReason.removeAt(2);
  }


  //REMOVE SPECIFIC ELEMENT;
  busyReason.removeWhere((element) => element == "Fiesta");
  //busyReason.firstWhere((element) => element == )*/

  //FIND FIRST ELEMENT WHICH STARTS WITH F



  //JOIN
  var reason = busyReason.join("/");

  // RETURNS LIKE JSON
  var mapReasons = busyReason.asMap();





  //AS MAP






}