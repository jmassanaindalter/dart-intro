// ANONYM FUNCTIONS
//ABSTRACT CLASSES

// LIBRARIES
import 'dart:io';

import "lib/areas.dart" as areas;
import 'package:intl/intl.dart';

void main() async{

  var today = DateTime.now();

  print("TODAY: $today");
  print("YEAR: ${today.year}");
  print("MONTH ${today.month}");
  print("DAY ${today.day}");

  var newDate = today.add(Duration(days:10));

  print("NEW DATE: ${newDate}");

  var diff = newDate.difference(today);

  print("DIFF:$diff" );
  print("DIFF DAYS:${diff.inDays}");
  print("DIFF HOURS: ${diff.inHours}");


  //COMPARE DATES:

  newDate.isBefore(today) ? print("NEW DATE IS BEFORE TODAY") :  print("NEW DATE IS AFTER TODAY");



  String formattedDate = DateFormat('yyyy-MM-dd – HH:mm').format(today);

  print("FORMATTED DATE: $formattedDate");



}





