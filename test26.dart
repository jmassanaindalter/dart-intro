// ANONYM FUNCTIONS
//ABSTRACT CLASSES

// LIBRARIES AND ASYNC

// ACCESS TO https://pub.dev

//install some packages


//JSON placeholder
//JSON placeholder

//DART WORKS IN A SINGLE THREAD

import 'package:dio/dio.dart' as dio;
import 'package:http/http.dart' as http;
Future<void> main() async {
  print("Performing request");
  //await getStatusCode();
  getStatusCode();
  print("......");
}

//WHY THE POINTS GET FIRST

Future<int> getStatusCode() async {
    http.Response response = await http.get(Uri.parse("https://jsonplaceholder.typicode.com/todos/1"));

    print("RESPONSE STATUS CODE: ${response.statusCode}");

    if(response.statusCode == 200){
      print("REQUEST WAS SUCCESSFUL");
      return response.statusCode;
    }else{
      print("REQUEST RETURNED AN ERROR");
      return response.statusCode;
    }
}







