// ANONYM FUNCTIONS
//ABSTRACT CLASSES

// A CLOUSURE IS AN OBJECT THAT HAS ACCESS TO THE LEXIC SCOPE


//SOURCE MEDIUM



void main(){
  var uri = "https://www.indalter.com/politica-cookies";
  var uriWrong = "https://www.indalter.com/politica cookies";


  var encoded =  Uri.encodeFull(uri);
  var decodeFull = Uri.decodeFull(uri);

  var encodedBad =  Uri.encodeFull(uriWrong);
  var decodeFullBad = Uri.decodeFull(uriWrong);

  //ENCODE OR DECODE

  print("Encoded: $encoded");
  print("decoded: $decodeFull");

  print("Encoded bad: $encodedBad");
  print("decoded bad: $decodeFullBad");


  var decodedFromEncodedBad = Uri.decodeComponent(encodedBad);
  var decodedFromEncoded = Uri.decodeComponent(encoded);

  print("DECODED FROM ENCODED BAD: $decodedFromEncodedBad");
  print("DECODED FROM ENCODED: $decodedFromEncoded");


  //CREATE URI AND ACCESS TO ITS PROPERTIES

  var url = Uri.parse(uri);

  print("URL HOST ${url.host}");
  print("URL SCHEME ${url.scheme}");
  print("URL PATH ${url.path}");

  // CREATE URI STEP BY STEP;

  var urlCreated = Uri(
    scheme:"https",
    host:"www.indalter.com",
    path:"politica-cookies"
  );

  print("URL CREATED $urlCreated");


  //USABLE FOR SERVER SIDE AND VALIDATING URIS;

}





