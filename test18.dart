// ANONYM FUNCTIONS
//CLOSURES

// A CLOUSURE IS AN OBJECT THAT HAS ACCESS TO THE LEXIC SCOPE


class Car{

  String? brand;
  String? type;
  String? color = "Undefined color";
  int cvs;



  Car({this.brand =" unbranded", this.type="simple car",this.color="undefined color", this.cvs=100});
  /*Car(String brand, String type){
    this.brand = brand;
    this.type = type;
  }*/

  factory Car.fromJson(Map jsonMap){
    return Car(
      brand:jsonMap["brand"] ?? "No brand",
      type:jsonMap["type"] ?? "No type defined",
      color: jsonMap["color"] ?? "Undefined color",
      cvs: jsonMap["cvs"] ?? 100
    );
  }


  paintCar(String color){
    color = "green";
  }

  tuneCar(int addedCvs){
    cvs += addedCvs;
  }




  @override
  String toString() {
    return "I have an $brand with $cvs cvs which is a $type and its color is $color";
  }

}

int calculateArrow() => 6*7;

void main(){
    //Car car1 = new Car();
    //Car car2 = Car();

    Car car3 = Car(brand:"Opel",type:"sport car",cvs:300);

    print("CAR1: $car3");


    var carJson = {
      "brand":"Fiat",
      "type":"van",
      "cvs":90
    };

    Car car4 = Car.fromJson(carJson);

    print("${car3.toString()}");
    print("$car4");

    car4.tuneCar(100);
    car4.paintCar("black");


    print("CAR4 tuned: $car4");
}






