// TEST OPERATORS
//FOR CYCLE AND LOOPS
void main(){

  List<String> colors = ["Red","Yellow","Green","Orange","Brown","Black","Purple","Blue","Grey"];


  //FOR LOOP

  for(int pos = 0; pos < colors.length; pos++){
    print("COLORS IN POSITION $pos: ${colors[pos]}");
  }

  for(String color in colors){
    print("COLOR is $color");
  }


  //WHILE LOOP
  var i = 0;
  while ( i < 10){
    i++;
    i = i+1;
    print("INCREMENTO EN 1 $i");
  }

  //Exercises table de multipicar del 2

  i = 0;
  do{
    print("THIS IS DO WHILE");
    i++;
  }while(i < 10);
}