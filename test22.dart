// ANONYM FUNCTIONS
//ABSTRACT CLASSES

// A CLOUSURE IS AN OBJECT THAT HAS ACCESS TO THE LEXIC SCOPE


//SOURCE MEDIUM

abstract class Animal{
    animal() => print("I'm an animal");

}

abstract class Mammal extends Animal{
    mammal() => print("I'm a mammal");
}

abstract class Bird extends Animal {
    bird() => print("I'm a bird");
}

abstract class Fish extends Animal{
    fish() => print("I'm a fish");
}


mixin  Walk {
    walk()=> print("I can walk");
}

mixin Swim {
    swim()=> print("I can swim");
}

mixin Fly {
    fly()=> print("I can fly");
}


class Bat extends Mammal with Walk,Fly{
    bat()=> print("I'm a bat");
}

class Duck extends Bird with Walk,Fly, Swim{
    duck()=> print("I'm a duck");

}

class Shark extends Fish with Swim{
    shark()=> print("I'm a shark");
}

void main(){
  Bat bat = Bat();

  bat.bat();
  bat.animal();
  bat.mammal();
  bat.walk();
  bat.fly();

}






