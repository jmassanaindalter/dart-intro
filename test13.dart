// FUNCTIONS AND PARAMETERS

void main(){
  sum(a:3);

  multiplyByTwo(parameter:4);



}

int sub(a,b){
  return a-b;
}

//NAMED PARAMETERS
int multiply(int a, int b){
    return a*b;
}

//NAMED PARAMETERS WITH DEFAULT VALUE
int sum({int a  = 3, int b = 4}){

  b ??=1; // SI LA VARIABLE ES NULA EL DEFAULT ES 1

  return a+b;
}

//REQUIRED PARAM
double multiplyByTwo({required double parameter}){
  return parameter * 2;
}





