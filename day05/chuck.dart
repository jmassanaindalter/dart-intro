

/*
{ "categories":[],
  "created_at":"2020-01-05 13:42:21.179347",
  "icon_url":"https://assets.chucknorris.host/img/avatar/chuck-norris.png",
  "id":"JJAAmEyFQcqlWInDZcvTJg","updated_at":"2020-01-05 13:42:21.179347",
  "url":"https://api.chucknorris.io/jokes/JJAAmEyFQcqlWInDZcvTJg",
  "value":"CHuck Norris' Mother has a tatoo of Chuck Norris on her right bicep."}
 */
import 'dart:convert';

import 'package:http/http.dart' as http;


class Joke{

  List<dynamic>? categories;
  String? createdAt;
  String? iconUrl;
  String? id;
  String? url;
  String? value;

  Joke({this.categories,this.createdAt,this.iconUrl,this.id,this.url,this.value});

  factory Joke.fromJson(Map<String,dynamic> json){
    return Joke(
        categories:json["categories"] != null ? List.from(json['categories'].map((category) {
            return category;
        }).toList()) : [],
        createdAt: json["created_at"] ?? "",
        iconUrl: json["icon_url"] ?? "",
        id: json["id"] ?? 100,
        url: json['url'],
        value: json['value']);
  }

}

 Future<void> main() async {

  var catUrl = Uri.parse("https://api.chucknorris.io/jokes/categories");

  http.Response response = await http.get(catUrl);

  var listCategories = jsonDecode(response.body);
  //List.from(response.body);

  print("Response body ${listCategories.runtimeType}");




  for(var i = 0; i < listCategories.length; i++) {
    http.Response response = await http.get(
        Uri.parse("https://api.chucknorris.io/jokes/random?category=${listCategories[i]}"));



    var bodyEncoded = jsonDecode(response.body);


    Joke joke = Joke.fromJson(bodyEncoded);
    print("JOKE ");
    print("JOKE ${joke.categories}  - ${joke.value}");

    await Future.delayed(const Duration(seconds:3));
  }

}
//HACER UN PROGRAMA QUE HAGA LO SIGUIENTE

//