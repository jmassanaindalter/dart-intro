// https://jsonplaceholder.typicode.com/users/1

/*
{
  "id": 1,
  "name": "Leanne Graham",
  "username": "Bret",
  "email": "Sincere@april.biz",
  "address": {
    "street": "Kulas Light",
    "suite": "Apt. 556",
    "city": "Gwenborough",
    "zipcode": "92998-3874",
    "geo": {
      "lat": "-37.3159",
      "lng": "81.1496"
    }
  },
  "phone": "1-770-736-8031 x56442",
  "website": "hildegard.org",
  "company": {
    "name": "Romaguera-Crona",
    "catchPhrase": "Multi-layered client-server neural-net",
    "bs": "harness real-time e-markets"
  }
}
 */

import 'dart:convert';

import 'package:http/http.dart' as http;


class Geo{
  double? lat;
  double? lng;

  Geo({this.lat,this.lng});

  factory Geo.fromJson(Map<String,double> json){

    return Geo(lat: json["lat"] ?? 0,lng: json["lng"] ?? 0);
  }
}

class Address{
  String? street;
  String? suite;
  String? city;
  String? zipcode;
  Geo? geo;

  Address({this.street,this.suite,this.city,this.zipcode,this.geo});

  factory Address.fromJson(Map<String,dynamic> json){
    return Address(
      street:json["street"] ?? "",
      suite:json["suite"] ?? "",
      city:json["city"] ?? "",
      zipcode:json["zipcode"] ?? "",
      geo: json["geo"] != null ? Geo.fromJson(json["geo"]) : null
    );
  }
}


class Company{
  String? name;
  String? catchPhrase;
  String? bs;


  Company({this.name,this.catchPhrase,this.bs});

  factory Company.fromJson(Map<String,dynamic> json){
    return Company(
        name:json["name"] ?? "",
        catchPhrase:json["catchPhrase"] ?? "",
        bs:json["city"] ?? "",
    );
  }
}



class User{
  int id;
  String name;
  String username;
  String email;
  Address? address;
  String? phone;
  String? website;
  Company? company;

  User({this.id = -1,
        this.name = "",
        this.username = "",
        this.email = "",
        this.address,
        this.phone = "",this.website = "",this.company});

  factory User.fromJson(Map<String,dynamic> json){
    return User(
        id:json["id"] ?? -1,
        name: json["name"] ?? "",
        username: json["username"] ?? "",
        email: json["email"] ?? "",
        address: json["address"] != null ? Address.fromJson(json["address"]) : null,
        phone: json["phone"] ?? "",
        website: json["website"] ?? "",
        company: json["company"] != null ? Company.fromJson(json["company"]) : null,
    );
  }


}


  Future<void> main() async {

    /*for(var i = 1; i <10; i++) {
      http.Response response = await http.get(
          Uri.parse("https://jsonplaceholder.typicode.com/users/$i"));

      //print("RAW JOJE: ${response.body}");

      var bodyEncoded = jsonDecode(response.body);

      //print("BODY ENCODED: ${bodyEncoded.runtimeType}" );

      User user = User.fromJson(bodyEncoded);

      print("USER ${user.name}");
      print("COMPANY ${user.company?.name}");

      await Future.delayed(Duration(seconds:3));
    }*/


    http.Response responseList = await http.get(
        Uri.parse("https://jsonplaceholder.typicode.com/users/"));

    List<dynamic> decoded = List.from(jsonDecode(responseList.body));

    List<User> users =  decoded.map((user)=>User.fromJson(user)).toList();

    users.forEach( (user) =>{
      print("user.name: ${user.name}")
    });


  }

