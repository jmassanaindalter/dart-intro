// ANONYM FUNCTIONS
//ABSTRACT CLASSES

// A CLOUSURE IS AN OBJECT THAT HAS ACCESS TO THE LEXIC SCOPE


class Transport {
    int wheels = 4;
    String fuel = "Gasoline";
    int speed = 100;

    void run(){
      print("Running at $speed Km/h");
    }
}


class Car extends Transport{
    bool hasTurbo = false;
    int doors = 100;

    void infoCar(){
      print("THE CAR HAS TURBO $hasTurbo");

    }
}

class Motorbike extends Transport{
    bool isOffroad = false;

    @override
    int wheels = 2;

    void infoMotorbike(){
      print("THE BIKE IS OFFROAD $isOffroad");
    }
}




void main(){
    Car car1 = Car();
    Motorbike bike = Motorbike();

}






